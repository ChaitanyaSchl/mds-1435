package com.scholastic.mds.test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.bson.BSONObject;
import org.junit.Assert;
import org.junit.Test;

import com.mongodb.DBObject;
import com.scholastic.mds.util.mds1435Utils;


public class TestMDS1435CoverImageAtWorkLevel {
	
	public static Properties CONFIG=null;
	private static final Logger LOG = LogManager.getLogger(TestMDS1435CoverImageAtWorkLevel.class);


	@Test
	public void checkMresID() throws UnknownHostException, ParseException, IOException {

		CONFIG = new Properties();
		FileInputStream fn = new FileInputStream(System.getProperty("user.dir")+"//prodIDs.properties");
		CONFIG.load(fn);
		
		String bothImgs = CONFIG.getProperty("both");
		String mresId = "";
		String onlyImg = CONFIG.getProperty("one");
		String imageId = "";
		String noImg = CONFIG.getProperty("none");
		String noId = "";
		String status = "PASSED";
		
		String id = "";

//		List<String> list = mds1435Utils.findAllIds();
		List<String> list = new ArrayList<String>();
		list.add(bothImgs);
		list.add(onlyImg);
		list.add(noImg);
		
		int rowCount = list.size();

		for (int i = 0; i <= 2; i++) {
			id = list.get(i);
			
			
			// System.out.println("ID ::: i " + id + " - " + i);
			// String id = "PCD_WORK_1100";

			if (id.contains("PCD_WORK_")) {
				DBObject jResp = mds1435Utils.getProductFromDB(id);
				// System.out.println(jResp.toString());

				BSONObject values = (BSONObject) jResp.get("values");
				String coverImg = (String) values.get("COVER_IMAGE_PRODUCT_ID");
				// System.out.println("Cover Image ID: " + coverImg);
				String coverImgMres = (String) values
						.get("Cover_Image_Mres_Product_ID");
				// System.out.println("Cover Image MRES ID: " + coverImgMres);

				if (coverImg != null && coverImgMres != null) {
					mresId = coverImgMres;
				} else if (coverImg != null && coverImgMres == null) {
					imageId = coverImg;
				} else if (coverImg == null && coverImgMres == null) {
					noId = null;
				}
			}

		}

		HashMap<String, String> map = new HashMap<String, String>();
		map.put(bothImgs, mresId);
		map.put(onlyImg, imageId);
		map.put(noImg, noId);

		for (String key : map.keySet()) {
			String retId = mds1435Utils.validateCovImg(key);
			if (map.containsKey(key)) {
				String value = map.get(key);
				if (StringUtils.equals(retId, value)) {
					LOG.info(key + " - Validation PASSED");
				} else {
					LOG.error(key + " - Validation FAILED");
					status = "FAILED";
				}
			} else {
				LOG.error(key.equals(noImg) ? key + " - Validation PASSED" : key + " - Validation FAILED");
				status = "FAILED";
			}
		}
		
		String ids = mresId + ", " + imageId + ", " + noId;
		if (status.equals("FAILED")){
			//sendEmailUponFailure.sendEmail(ids);
			Assert.fail("Tested " + ids + ". Test FAILED");
		}
		Assert.assertEquals("Tested " + ids + ". Test ran SUCCESSFULLY ", status, "PASSED");

	}

}
