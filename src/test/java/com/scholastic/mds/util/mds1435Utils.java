package com.scholastic.mds.util;
import java.io.File;
import java.io.FileInputStream;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;


public class mds1435Utils {
	
	public static final RestTemplate restTemplate = new RestTemplate();
	public static DBCollection dbProdCollection;
	public static DB db;
	
	public static final DBObject getProductFromDB(String id) {
		Mongo mongo = new Mongo("mdm.qa1.ss-prod.us-east-1.schl.local", 27017);
		db = mongo.getDB("WORK_EN_US");
		DBCollection productCollection = db.getCollection("product");

		BasicDBObject allQuery = new BasicDBObject();

		allQuery.append("_id", id);
		DBObject objectDB = productCollection.findOne(allQuery);

		// String result = objectDB.toString();

		mongo.close();
		return objectDB;
	}

	public static String validateCovImg(String id) throws ParseException {

		String covImgId = null;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.set("Authorization",
					"Bearer 8b802a4f8fe513a6501f2abdab448275");
			HttpEntity<String> request = new HttpEntity<String>(headers);
			ResponseEntity<String> response = null;

			// response =
			// restTemplate.exchange("https://nonprod.api.scholastic.com/qa/ondemand/2.0/product/2.0/"
			// + id, HttpMethod.GET, request,String.class);
			// System.out.println("ID :::: " + id);
			response = restTemplate.exchange(
					"http://10.44.150.117:8080/ondemand/work/2.0/" + id,
					HttpMethod.GET, request, String.class);
			// System.out.println("Response before :::: " + response);

			String resp = response.toString();
			resp = resp.substring(resp.indexOf(",") + 1,
					resp.indexOf(",{Server")); // {Access

			// System.out.println("Response from API :::: " + resp);

			JSONObject jsonObject = (JSONObject) new JSONParser().parse(resp);

			JSONObject work = (JSONObject) jsonObject.get("work");
			JSONObject data = (JSONObject) work.get("data");

			try {
				covImgId = (String) data.get("coverImageProductId");
			} catch (Exception e) {
			}
		} catch (Exception e) {
		}

		return covImgId;
	}

	public static HashMap<String, String> prodAttrList() {
		HashMap<String, String> attrList = new HashMap<String, String>();

		try {
			File file = new File("productConfig.properties");
			FileInputStream fileInput = new FileInputStream(file);
			Properties properties = new Properties();
			properties.load(fileInput);
			fileInput.close();

			for (String key : properties.stringPropertyNames()) {
				String value = properties.getProperty(key);
				// System.out.println(key + " - " + value);
				attrList.put(key, value);
			}

		} catch (Exception e) {
		}
		return attrList;
	}

	public static DBCollection loadMongoForIds() throws UnknownHostException {

		MongoClient mongo;
		if (dbProdCollection == null) {
			mongo = new MongoClient("mdm.qa1.ss-prod.us-east-1.schl.local",
					27017);
			DB db = mongo.getDB("WORK_EN_US");
			// db.authenticate("username", "password".toCharArray());
			dbProdCollection = db.getCollection("product");
		}
		return dbProdCollection;
	}

	public static List<String> findAllIds() throws UnknownHostException {
		loadMongoForIds();
		return dbProdCollection.distinct("_id");
	}

}
